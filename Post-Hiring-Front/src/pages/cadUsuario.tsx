import { useEffect, useState } from "react";
import "../styles/cadUsuario.css";
import api from '../services/api'
import { useNavigate } from "react-router-dom";

export function CadUsuario() {
  const [listaSetor, setListaSetor] = useState<[]>([])
  const [user_id, setUserSectorid] = useState('')
  const [userName, setuserName] = useState("");
  const [userNickName, setUserNickname] = useState("");
  const [userDate, setuserDate] = useState("");
  const [userSetor, setuserSetor] = useState("");
  const [userEmail, setuserEmail] = useState("");
  const navigate = useNavigate()

  useEffect(() => {
    loadDataTable()
  }, [])
  async function loadDataTable() {
    const data = await api.get("/api/sector/listAllSector")
    console.log(data.data)
    setListaSetor(data.data)
  }

  async function cadUser(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    const res = await api.post("/api/user/insertUser/userAssociate", {
      nickname: userNickName,
      user_name: userName,
      birth_date: userDate,
      sector_id: user_id,
      email: userEmail,
    }
    );
    navigate("/home")
  }

  return (
    <>
      <section className="cadUsuario">
        <div className="area-usuario">
          <div className="divUsuario">
            <button onClick={() => navigate('/listaUsuario')}>Voltar</button>
            <h1>Cadastro Usuário</h1>
            <form onSubmit={cadUser}>
            <select onChange={event => setUserSectorid(event.target.value)}>
                <option>Selecione um Setor</option>
                {listaSetor.map((item: any, index: number) => (<option key={index} value={item.sector_id}>{item.sector_name}</option>))}
              </select>
              <input
                type="text"
                name="name"
                id="name"
                placeholder="Insira o nome completo"
                value={userName}
                onChange={(event) => setuserName(event.target.value)}
              />
              <input
                type="text"
                name="nickname"
                id="nickname"
                placeholder="Insira o nome de usuário"
                value={userNickName}
                onChange={(event) => setUserNickname(event.target.value)}
              />
              <input
                type="date"
                name="date"
                id="date"
                placeholder="Insira data de nascimento"
                value={userDate}
                onChange={(event) => setuserDate(event.target.value)}
              />
              <input
                type="text"
                name="setor"
                placeholder="Insira o setor"
                value={userSetor}
                onChange={(event) => setuserSetor(event.target.value)}
              />
              <input
                type="email"
                name="email"
                placeholder="Insira o email"
                value={userEmail}
                onChange={(event) => setuserEmail(event.target.value)}
              />
              <input type="submit" className="button" value="Cadastrar" />
            </form>
          </div>
        </div>
        <footer><h3>Copyright - Todos direitos reservados</h3></footer>
      </section>
    </>
  );
}
export default CadUsuario;