import "../stylesInterface/home.css";
import React, { useState, useContext, useEffect } from "react";
import api from "../services/api";

export function Home() {
  interface teste {
    setores: setor[];
  }

  interface setor {
    description: String;
    document_content: String;
    document_id: number;
    material_link: String;
    sector_id: number;
    sector_name: String;
    title: String;
  }

  const [Home, setHome] = useState<setor[]>([] as setor[]);

  async function listHome() {
    const user = JSON.parse(sessionStorage.getItem("user") ?? '')
  
    const document_id_session = user.document_id

    const res = await api.get(`api/documents/listOneDocuments/${document_id_session}`);

    console.log(res.data.documents)
    setHome(res.data.documents);
  }

  useEffect(() => {
    listHome();
  }, []);

  if (!Home.length) {
    return <div>No data</div>;
  }

  return (
    <>
      <br />
      <br />
      <br />

      <section className="home">
        <div className="div">
          {Home.map((home, index) => (
            <div key={index}>
              <h1>
                {home.sector_name} : {home.title}
              </h1>
              <p>{home.description}</p>
              <p>{home.document_content}</p>
              <a href="{home.material_link}">{home.material_link}</a>
              <div className="divider"></div>
            </div>
          ))}
        </div>
      </section>
      <footer><h3>Copyright - Todos direitos reservados</h3></footer>
    </>
  );
}

export default Home;
