import '../styles/editDocument.css'
import {useLocation, useNavigate} from 'react-router-dom';
import {useState } from 'react';
import api from '../services/api';

export function EditDocument() {
    const dados: any = useLocation().state
    
    const [title, setTitle] = useState(dados.title);
    const [materialLink, setMaterialLink] = useState(dados.material_link);
    const [document_content, setdocument_content] = useState(dados.document_content);
    const navigate = useNavigate()

    async function edtDoc(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        const res = await api.put("/api/documents/updateDocuments/"+dados.document_id, {
                title: title,
                material_link: materialLink,
                document_content: document_content,
                sector_id: dados.sector_id 
            }
        ); 

         navigate("/home")    
    }
    return (
        <>
            <section className='editDocument'>
                <div className="area-document">
                    <div className="divDocument">
                        <h1>Edição de Documento</h1>
                        <form onSubmit={edtDoc}>
                            <input 
                            type="text" 
                            name='material' 
                            value={title}
                            onChange={(event)=> setTitle(event.target.value)}
                            />
                            <input 
                            name="material" 
                            className='material'
                            value={materialLink}
                            onChange={(event)=> setMaterialLink(event.target.value)}>
                            </input>
                            <textarea 
                            name="material" 
                            className='material'
                            value={document_content}
                            rows={13}
                            onChange={(event)=> setdocument_content(event.target.value)}>
                            </textarea>
                            <input type="submit" className='button' value="Editar"/>
                        </form>
                    </div>
                </div>
                <footer><h3>Copyright - Todos direitos reservados</h3></footer>
            </section>
        </>
    )
}

export default EditDocument;