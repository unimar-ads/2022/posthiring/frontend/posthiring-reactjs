import '../stylesInterface/menu.css'
import { useState, useEffect } from 'react';
import { IoMdMenu } from "react-icons/io";
import { useNavigate, Link } from 'react-router-dom';
import {FaWhatsapp} from 'react-icons/fa'
import {FaMailBulk} from 'react-icons/fa'
import {FaInstagram} from 'react-icons/fa'

export function Menu() {
  const navigate = useNavigate()
  const [active, setActive] = useState(false)
    
  const user = JSON.parse(sessionStorage.getItem("user") ?? '')
  
  const user_adm = user.is_supervisor

  function logout() {
    navigate('/login')
  }

  function home() {
    navigate('/home')
  }

  return (
    <>
      <nav className="upper-nav">
        <i>
          <button onClick={() => setActive(!active)}>
            <div className='toggle icon-menu'><IoMdMenu style={{ height: 30, width: 30, cursor: "pointer", borderRadius: 10 }} /></div>
          </button>
        </i>
        <div onClick={home} className='logo'>Post<strong>Hiring</strong>.</div>
      </nav>
      <aside className={`${!active ? "hidden" : "shown"}`}>
        <ul>
          <li><Link to="/home">Home</Link></li>
          <li><Link to="/perfil">Perfil</Link></li>

          { user_adm && 
            <>
              <li><Link to="/listaUsuario">Usuário</Link></li>
              <li><Link to="/listaDocument">Documento</Link></li>
              <li><Link to="/listaSetor">Setor</Link></li>
            </>
          }
          <li><div className='divider'></div></li>
          <li><a className='log' onClick={logout}>Sair</a></li>
          <li className='icon'>
            <a href="https://criarmeulink.com.br/u/1667752463"><FaMailBulk/></a> 
            <a href="https://api.whatsapp.com/send?phone=5518998065592&text=Ol%C3%A1!!%20Gostaria%20de%20falar%20sobre%20o%20PostHiring.."> <FaWhatsapp/></a>
            <a href="https://www.instagram.com/posthiring/"> <FaInstagram/></a>
          </li>
        </ul>
      </aside>
    </>
  )
}

export default Menu;

