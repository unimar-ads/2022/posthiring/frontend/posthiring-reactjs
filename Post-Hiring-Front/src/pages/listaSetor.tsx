import api from '../services/api'
import { useNavigate } from 'react-router-dom'
import { useState, useEffect } from 'react'
import { AxiosError } from 'axios'

interface sector {
  sector_id: number,
  sector_name: string,
  description: string
}

export function ListaSetor() {
  const navigate = useNavigate()
  const [data, setData] = useState<sector[]>([])

  useEffect(() => {
    loadDataTable()
  }, [])

  async function loadDataTable() {
    const response = await api.get('/api/sector/listAllSector')
    console.log(response.data)
    setData(response.data)
  }

  async function handleDelete({ sector_id }: sector) {
    try {
      await api.delete(`/api/sector/deleteSector/${sector_id}`)
      loadDataTable()
      alert('Setor excluído com sucesso!')
    } catch (ex: unknown) {
      if (ex instanceof AxiosError) {
        alert(`ERRO: ${ex?.response?.data.mensagem}`)
      }
    }
  }

  function EdtSetor({ sector_id }: sector) {
    navigate(`/CadSetor/${sector_id}`, { replace: true })
  }

  function renderTable() {
    try {
      return (
        <div className='divCadDocument'>
          <section className='cadDocument'>
            <div className='formGroup'>
              <button className='btn-new' onClick={() => navigate('/cadSetor')}>Novo Setor</button>
              {data.map((item: sector, index: number) => (
                <div className='components' key={index}>
                  <h1>{item.sector_name}</h1>
                  <p>{item.description}</p>
                  <div className='btn'>
                    <button className='blue' onClick={() => EdtSetor(item)}>Editar</button>
                    <button className='red' onClick={() => handleDelete(item)}>Excluir</button>
                  </div>
                </div>
              ))}
              <br />
            </div>
          </section>
        </div>
      )
    } catch (error) {
      console.log('error', error)
    }
  }

  return (
    <>
      <br />
      <br /><br />
      {renderTable()}
      <footer><h3>Copyright - Todos direitos reservados</h3></footer>
    </>
  )
}

export default ListaSetor;