import { useEffect, useState } from 'react';
import '../styles/perfil.css'
import api from "../services/api";

export function Perfil() {

    interface Perfil {
        user_id: number;
        user_name: String;
        birth_date: Date;
        sector_id: number;
        is_supervisor: boolean;
        nickname: String;
        email: String;
        status: boolean;
    }

    const [perfil, setPerfil] = useState<Perfil>({} as Perfil);

    async function getPerfil() {
        const user = JSON.parse(sessionStorage.getItem("user") ?? '')
        const user_id_session = user.user_id

        const res = await api.get(`api/user/listOneUser/${user_id_session}`);
        setPerfil(res.data.users[0]);
        console.log(res.data.users[0])
    }
    
    useEffect(() => {
        getPerfil();
    }, []);

    return (
        <>
            <div className='divOnePerfil'>
                <section className='perfil'>
                    <div className="area-perfil">
                        <div className="divPerfil">
                            <h1>Perfil de Usuário</h1>
                            <form>
                                <label>{perfil.user_name}</label>
                                <label>{perfil.nickname}</label>
                                <label>{perfil.email}</label>
                                <label>{perfil.sector_id}</label>
                                {/* <label>{perfil.birth_date}</label> */}
                                <label>{perfil.status}</label>
                                <label>{perfil.is_supervisor}</label>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
            <footer><h3>Copyright - Todos direitos reservados</h3></footer>
        </>
    )
}

export default Perfil;