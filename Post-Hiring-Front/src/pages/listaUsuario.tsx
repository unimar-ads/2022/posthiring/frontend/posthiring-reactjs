import api from '../services/api'
import { useNavigate } from 'react-router-dom'
import { useState, useEffect } from 'react'

export function ListaUsuario() {
    const navigate = useNavigate()

    interface usuario {
        user_id: number,
        user_name: String,
        birth_date: String,
        sector_id: number,
        is_supervisor: String,
        nickname: String,
        email: String,
        password: number
    }

    const [Usuario, setListUsuario] = useState<usuario[]>([] as usuario[]);

    async function ListUsuario() {
        const res = await api.get("/api/user/listAllUser");
        setListUsuario(res.data.users)
        console.log(res.data.users)
    }

    useEffect(() => {
        ListUsuario();
    }, []);

    function EdtUser(dados: usuario) {
        navigate("/editUsuario", {
            state: {
                user_id: dados.user_id,
                user_name: dados.user_name,
                birth_date: dados.birth_date,
                sector_id: dados.sector_id,
                is_supervisor: dados.is_supervisor,
                nickname: dados.nickname,
                email: dados.email,
                password: dados.password
            }
        })
        console.log(dados.user_name)
    }

    return (
        <>
            <br /><br />
            <br />
            <div className='divCadDocument'>
                <section className='cadDocument'>
                    <div className='formGroup'>
                        <button className='btn-new' onClick={() => navigate("/cadUsuario")}>Novo Usuario</button>
                        {Usuario.map((use: any, index: number) =>
                            <div className='components' key={index}>
                                <h1>{use.user_name}</h1>
                                <p>{use.nickname}</p>
                                <p>{use.birth_date}</p>
                                <p>{use.sector_id}</p>
                                <p>{use.is_supervisor}</p>
                                <p>{use.email}</p>
                                <p>{use.password}</p>
                                <div className='btn'>
                                    <button className='blue' onClick={() => EdtUser(use)}>Alterar</button>
                                </div>
                            </div>
                        )}
                    </div>
                </section>
            </div>
            <footer><h3>Copyright - Todos direitos reservados</h3></footer>
        </>
    )
}

export default ListaUsuario;