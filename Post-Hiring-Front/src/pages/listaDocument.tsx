import React, { useState, useEffect } from 'react';
import api from '../services/api';
import { useNavigate } from 'react-router-dom';
import '../styles/cadDocument.css'

export function ListaDocument() {
    const navigate = useNavigate()

    interface setor {
        description: String,
        document_content: String,
        document_id: number,
        material_link: String,
        sector_id: number,
        sector_name: String,
        title: String,
    }

    const [Document, setListDocument] = useState<setor[]>([] as setor[]);

    async function ListDocument() {
        const res = await api.get("/api/documents/listAllDocuments");
        setListDocument(res.data)
        console.log(res.data)
    }

    useEffect(() => {
        ListDocument();
    }, []);

    function EdtDoc(dados: setor) {
        navigate("/EditDocument", {
            state: {
                description: dados.description,
                document_content: dados.document_content,
                document_id: dados.document_id,
                material_link: dados.material_link,
                sector_id: dados.sector_id,
                sector_name: dados.sector_name,
                title: dados.title,
            }
        })

    }
    async function Edtdelete(dados: setor) {
        const res = await api.delete("api/documents/deleteDocuments/" + dados.document_id)
        ListDocument();
        navigate("/home")
    }

    return (
        <>
            <br />
            <br /><br />
            <div className='divCadDocument'>
                <section className='cadDocument'>
                    <div className='formGroup'>
                        <button className='btn-new' onClick={() => navigate("/cadDocument")}>Novo Documento</button>
                        {Document.map((doc: any, index: number) =>
                            <div className='components' key={index}>
                                <h1>{doc.sector_name} : {doc.title}</h1>
                                <p>{doc.description}</p>
                                <p>{doc.material_link} </p>
                                <div className='btn'>
                                    <button className='blue' onClick={() => EdtDoc(doc)}>Alterar</button>
                                    <button className='red' onClick={() => Edtdelete(doc)}>Excluir</button>
                                </div>
                            </div>
                        )}
                    </div>
                </section>
                <br />
            </div>
            <footer><h3>Copyright - Todos direitos reservados</h3></footer>
        </>
    )
}

export default ListaDocument;