import '../styles/editDocument.css'
import { useLocation, useNavigate } from 'react-router-dom';
import { useState } from 'react';
import api from '../services/api';

export function EditUsuario() {
    const dados: any = useLocation().state

    const [user_name, setUser_name] = useState(dados.user_name);
    const [birth_date, setBirth_date] = useState(dados.birth_date);
    const [sector_id, setSector_id] = useState(dados.sector_id);
    const [is_supervisor, setIs_supervisor] = useState(dados.is_supervisor);
    const [nickname, setNickname] = useState(dados.nickname);
    const [email, setEmail] = useState(dados.email);
    const navigate = useNavigate()

    async function edtUser(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        const res = await api.put("/api/user/updateUser/user/" + dados.user_id, {
            user_name: user_name,
            birth_date: birth_date,
            sector_id: sector_id,
            is_supervisor: is_supervisor,
            nickname: nickname,
            email: email,
            // sector_id: dados.sector_id
        }
        );
        navigate("/home")
    }

    return (
        <>
            <section className='editDocument'>
                <div className="area-document">
                    <div className="divDocument">
                        <h1>Edição de Usuário</h1>
                        <form onSubmit={edtUser}>
                            <input 
                            type="user_name" 
                            name='user_name' 
                            value={user_name}
                            onChange={(event)=> setUser_name(event.target.value)}
                            />
                            <input 
                            name="birth_date" 
                            className='birth_date'
                            value={birth_date}
                            onChange={(event)=> setBirth_date(event.target.value)}>
                            </input>
                            <input 
                            name="sector_id" 
                            className='sector_id'
                            value={sector_id}
                            onChange={(event)=> setSector_id(event.target.value)}>
                            </input>
                            <input 
                            name="is_supervisor" 
                            className='is_supervisor'
                            value={is_supervisor}
                            onChange={(event)=> setIs_supervisor(event.target.value)}>
                            </input>
                            <input 
                            name="nickname" 
                            className='nickname'
                            value={nickname}
                            onChange={(event)=> setNickname(event.target.value)}>
                            </input>
                            <input 
                            name="email" 
                            className='email'
                            value={email}
                            onChange={(event)=> setEmail(event.target.value)}>
                            </input>
                            <input type="submit" className='button' value="Editar"/>
                        </form>
                    </div>
                </div>
                <footer><h3>Copyright - Todos direitos reservados</h3></footer>
            </section>
        </>
    )
}


export default EditUsuario;