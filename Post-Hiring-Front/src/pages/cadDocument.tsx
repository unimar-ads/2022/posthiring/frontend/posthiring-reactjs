import { useState, useEffect } from 'react';
import '../styles/cadDocument.css'
import api from "../services/api"
import {useNavigate } from 'react-router-dom'

export function CadDocument() {
  const [listaSetor, setListaSetor] = useState<[]>([])
  const [docTitle, setdocTitle] = useState('')
  const [docMaterial, setdocMaterial] = useState('')
  const [docDescricao, setdocDescricao] = useState('')
  const [docsector_id, setDocSectorid] = useState('')
  const navigate = useNavigate()

  useEffect(() => {
    loadDataTable()
  }, [])
  async function loadDataTable() {
    const data = await api.get("/api/sector/listAllSector")
    console.log(data.data)
    setListaSetor(data.data)
  }

  async function CadDoc(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    const res = await api.post("/api/documents/insertDocuments", {
      title: docTitle,
      document_content: docDescricao,
      material_link: docMaterial,
      sector_id: docsector_id
    }
    );
    navigate("/listaDocument")
  }

  return (
    <>
      <section className='editDoc'>
        <div className="area-doc">
          <div className="divDoc">
            <h1>Cadastro Documento</h1>
            <form onSubmit={CadDoc}>
              <select onChange={event => setDocSectorid(event.target.value)}>
                <option>Selecione um Setor</option>
                {listaSetor.map((item: any, index: number) => (<option key={index} value={item.sector_id}>{item.sector_name}</option>))}
              </select>
              <input
                type="text"
                name='Title'
                id='Title'
                placeholder='Titulo do Material'
                value={docTitle}
                onChange={(event) => setdocTitle(event.target.value)}
              />
              <input
                type="text"
                name='material'
                id='material'
                placeholder='Link do Material'
                value={docMaterial}
                onChange={(event) => setdocMaterial(event.target.value)}
              />
              <textarea
                name="descricao"
                className='descricao'
                id='descricao'
                placeholder='Insira a descricao'
                value={docDescricao}
                rows={16}
                onChange={(event) => setdocDescricao(event.target.value)}
              />
              <input type="submit" className='button' value="Cadastrar" />
            </form>
          </div>
        </div>
      </section>
      <footer><h3>Copyright - Todos direitos reservados</h3></footer>
    </>
  )
}
export default CadDocument;