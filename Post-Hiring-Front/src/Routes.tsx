import {
  BrowserRouter as Router,
  Routes,
  Route,
  Outlet
} from "react-router-dom";

import { Login } from "./pages/login";
import { Home } from "./pages/home";
import { Menu } from "./pages/menu";
import { Perfil } from "./pages/perfil"

import { CadUsuario } from "./pages/cadUsuario";
import { CadDocument } from "./pages/cadDocument";
import { CadSetor } from "./pages/cadSetor";
import { EditDocument } from "./pages/editDocument";
import { EditUsuario } from "./pages/editUsuario";

import { ListaSetor } from "./pages/listaSetor";
import { ListaDocument } from "./pages/listaDocument";
import { ListaUsuario } from "./pages/listaUsuario";


export function AppRoutes() {
  return (
    <Router>
      <Routes>
        <Route path="*" element={<Login />} />
        <Route element={<> <Menu /> <Outlet /> </>}>
          <Route path="/home" element={<Home />} />
          <Route path="/menu" element={<Menu />} />
          <Route path="/perfil" element={<Perfil />} />
          
          <Route path="/cadUsuario" element={<CadUsuario />} />
          <Route path="/cadDocument" element={<CadDocument />} />
          <Route path="/editDocument" element={<EditDocument />} />
          <Route path="/editUsuario" element={<EditUsuario />} />


          <Route path="/cadSetor/:id" element={<CadSetor />} />
          <Route path="/cadSetor" element={<CadSetor />} />
          <Route path="/listaDocument" element={<ListaDocument />} />
          <Route path="/listaSetor" element={<ListaSetor />} />
          <Route path="/listaUsuario" element={<ListaUsuario />} />
        </Route>
      </Routes>
    </Router>
  )
}